# gnome-games' Portuguese translation.
# Copyright © 1998, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014 gnome-games
# Distributed under the same licence as the gnome-games package
# Nuno Ferreira <nmrf@rnl.ist.utl.pt>, 1998.
# Duarte Loreto <happyguy_pt@hotmail.com>, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013.
# Tiago Santos <tiagofsantos81@sapo.pt>, 2014 - 2016.
# Pedro Albuquerque <palbuquerque73@gmail.com>, 2015.
# Sérgio Cardeira <cardeira dot sergio at gmail dot com>, 2016.
# Hugo Carvalho <hugokarvalho@hotmail.com>, 2019 - 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: 3.12\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-sudoku/issues\n"
"POT-Creation-Date: 2023-06-25 11:56+0000\n"
"PO-Revision-Date: 2023-08-20 12:18+0100\n"
"Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>\n"
"Language-Team: Português <>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.3.2\n"

#: data/sudoku-window.ui:7
msgid "_New Puzzle"
msgstr "_Novo puzzle"

#: data/sudoku-window.ui:11
msgid "Create Custom Game"
msgstr "Criar um jogo personalizado"

#: data/sudoku-window.ui:15
msgid "_Clear Board"
msgstr "_Limpar tabuleiro"

#: data/sudoku-window.ui:21 data/print-dialog.ui:77
msgid "_Print"
msgstr "_Imprimir"

#: data/sudoku-window.ui:24
msgid "Print _Current Puzzle…"
msgstr "Imprimir o _puzzle atual…"

#: data/sudoku-window.ui:28
msgid "Print _Multiple Puzzles…"
msgstr "Imprimir _múltiplos puzzles…"

#: data/sudoku-window.ui:36
msgid "High_lighter"
msgstr "Rea_lce"

#: data/sudoku-window.ui:40
msgid "_Warnings"
msgstr "_Avisos"

#: data/sudoku-window.ui:46
msgid "_Help"
msgstr "_Ajuda"

#: data/sudoku-window.ui:50
msgid "_About Sudoku"
msgstr "_Acerca do Sudoku"

#: data/sudoku-window.ui:56 data/sudoku-window.ui:64
#: data/org.gnome.Sudoku.desktop.in:3 src/gnome-sudoku.vala:489
#: src/sudoku-window.vala:217
msgid "Sudoku"
msgstr "Sudoku"

#: data/sudoku-window.ui:72
msgid "Undo your last action"
msgstr "Desfazer a última ação"

#: data/sudoku-window.ui:84
msgid "Redo your last action"
msgstr "Refazer a última ação"

#: data/sudoku-window.ui:96
msgid "Go back to the current game"
msgstr "Voltar ao jogo atual"

#: data/sudoku-window.ui:107
msgid "Main Menu"
msgstr "Menu Principal"

#: data/sudoku-window.ui:115
msgid "Pause"
msgstr "Pausa"

#: data/sudoku-window.ui:127
msgid "Start playing the custom puzzle you have created"
msgstr "Começar a jogar o puzzle personalizado que criou"

#: data/sudoku-window.ui:153
msgid "Select Game Difficulty"
msgstr "Selecionar a dificuldade do jogo"

#: data/sudoku-window.ui:169 data/print-dialog.ui:58
msgid "Easy"
msgstr "Fácil"

#: data/sudoku-window.ui:180 data/print-dialog.ui:59
msgid "Medium"
msgstr "Médio"

#: data/sudoku-window.ui:191 data/print-dialog.ui:60
msgid "Hard"
msgstr "Difícil"

#: data/sudoku-window.ui:202 data/print-dialog.ui:61
msgid "Very Hard"
msgstr "Muito difícil"

#: data/sudoku-window.ui:212
msgid "Start Game"
msgstr "Iniciar o Jogo"

#: data/org.gnome.Sudoku.appdata.xml.in:7
msgid "GNOME Sudoku"
msgstr "Sudoku do GNOME"

#: data/org.gnome.Sudoku.appdata.xml.in:8 data/org.gnome.Sudoku.desktop.in:4
msgid "Test your logic skills in this number grid puzzle"
msgstr "Teste as suas capacidades lógicas neste puzzle numérico"

#: data/org.gnome.Sudoku.appdata.xml.in:10
msgid ""
"Play the popular Japanese logic game. GNOME Sudoku is a must-install for "
"Sudoku lovers, with a simple, unobtrusive interface that makes playing "
"Sudoku fun for players of any skill level."
msgstr ""
"Jogue o popular jogo de lógica japonês. O sudoku GNOME é essencial para "
"entusiastas de sudoku, com um ambiente simples e discreto, que torna este "
"jogo divertido para jogadores de qualquer nível."

#: data/org.gnome.Sudoku.appdata.xml.in:15
msgid ""
"Each game is assigned a difficulty similar to those given by newspapers and "
"websites, so your game will be as easy or as difficult as you want it to be."
msgstr ""
"A cada jogo é atribuída uma dificuldade equivalente às dadas em jornais e "
"páginas web, pelo que o seu jogo será tão fácil ou tão difícil quanto o "
"queira."

#: data/org.gnome.Sudoku.appdata.xml.in:20
msgid ""
"If you like to play on paper, you can print games out. You can choose how "
"many games you want to print per page and what difficulty of games you want "
"to print: as a result, GNOME Sudoku can act a renewable Sudoku book for you."
msgstr ""
"Se gosta de jogar em papel, pode imprimir os jogos. Pode escolher quantos "
"jogos quer imprimir por página e qual a dificuldade do jogo que quer "
"imprimir. Como tal, o GNOME Sudoku age como um livro de Sudoku renovável."

#: data/org.gnome.Sudoku.appdata.xml.in:30
msgid "A GNOME sudoku game preview"
msgstr "Uma previsão do jogo sudoku do GNOME"

#: data/org.gnome.Sudoku.appdata.xml.in:74
msgid "The GNOME Project"
msgstr "O Projeto GNOME"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Sudoku.desktop.in:6
msgid "magic;square;"
msgstr "magia;quadrado;"

#: data/org.gnome.Sudoku.gschema.xml:11
msgid "Difficulty level of sudokus to be printed"
msgstr "Nível de dificuldade dos jogos a imprimir"

#: data/org.gnome.Sudoku.gschema.xml:12
msgid ""
"Set the difficulty level of the sudokus you want to print. Possible values "
"are: \"easy\", \"medium\", \"hard\", \"very_hard\""
msgstr ""
"Defina o nível de dificuldade dos jogos que quer imprimir. Valores possíveis "
"são: \"Fácil\", \"Média\", \"Alta\" e \"Muito alta\""

#: data/org.gnome.Sudoku.gschema.xml:17
msgid "Number of Sudokus to print"
msgstr "Número de jogos a imprimir"

#: data/org.gnome.Sudoku.gschema.xml:18
msgid "Set the number of sudokus you want to print"
msgstr "Defina o número de jogos que quer imprimir"

#: data/org.gnome.Sudoku.gschema.xml:23
msgid "Number of Sudokus to print per page"
msgstr "Número de sudokus a imprimir por página"

#: data/org.gnome.Sudoku.gschema.xml:24
msgid "Set the number of sudokus you want to print per page"
msgstr "Defina o número de sudokus que quer imprimir por página"

#: data/org.gnome.Sudoku.gschema.xml:28
msgid "Warn about unfillable squares and duplicate numbers"
msgstr "Avisar sobre espaços impossíveis de preencher e números duplicados"

#: data/org.gnome.Sudoku.gschema.xml:29
msgid ""
"Displays a big red X in a square if it cannot possibly be filled by any "
"number and duplicate numbers are highlighted in red"
msgstr ""
"Mostra um grande X vermelho num espaço se este não puder ser preenchido por "
"qualquer número e os números duplicados são realçados a vermelho"

#: data/org.gnome.Sudoku.gschema.xml:33
msgid "Additionally warn when correct solution is violated or not earmarked"
msgstr ""
"Aviso adicional quando a solução correta for violada ou não for indicada"

#: data/org.gnome.Sudoku.gschema.xml:34
msgid ""
"Changes the background color of a square to red if the value does not match "
"puzzle solution, or if no earmark matches the puzzle solution"
msgstr ""
"Muda a cor de fundo de um quadrado para vermelho se o valor não corresponder "
"à solução do puzzle, ou se nenhuma indicação corresponder à solução do puzzle"

#: data/org.gnome.Sudoku.gschema.xml:38
msgid "Highlight row, column and square that contain the selected cell"
msgstr "Realçar linha, coluna e espaço que contêm a célula selecionada"

#: data/org.gnome.Sudoku.gschema.xml:42
msgid "Width of the window in pixels"
msgstr "Largura em pixéis da janela"

#: data/org.gnome.Sudoku.gschema.xml:46
msgid "Height of the window in pixels"
msgstr "Altura em pixéis da janela"

#: data/org.gnome.Sudoku.gschema.xml:50
msgid "true if the window is maximized"
msgstr "verdadeiro se a janela estiver maximizada"

#: data/org.gnome.Sudoku.gschema.xml:54
msgid "Initialize the earmarks with the possible values for each cell"
msgstr "Inicializar as indicações com os valores possíveis para cada célula"

#: data/print-dialog.ui:5
msgid "Print Multiple Puzzles"
msgstr "Imprimir vários puzzles"

#: data/print-dialog.ui:28
msgid "Number of Puzzles"
msgstr "Número de puzzles"

#: data/print-dialog.ui:41
msgid "Number of puzzles per page"
msgstr "Número de puzzles por página"

#: data/print-dialog.ui:54
msgid "Difficulty"
msgstr "Dificuldade"

#: lib/sudoku-board.vala:657
msgid "Unknown Difficulty"
msgstr "Dificuldade desconhecida"

#: lib/sudoku-board.vala:659
msgid "Easy Difficulty"
msgstr "Dificuldade fácil"

#: lib/sudoku-board.vala:661
msgid "Medium Difficulty"
msgstr "Dificuldade média"

#: lib/sudoku-board.vala:663
msgid "Hard Difficulty"
msgstr "Dificuldade alta"

#: lib/sudoku-board.vala:665
msgid "Very Hard Difficulty"
msgstr "Dificuldade muito alta"

#: lib/sudoku-board.vala:667
msgid "Custom Puzzle"
msgstr "Puzzle personalizado"

#. Help string for command line --version flag
#: src/gnome-sudoku.vala:72
msgid "Show release version"
msgstr "Mostrar versão de lançamento"

#. Help string for command line --show-possible flag
#: src/gnome-sudoku.vala:76
msgid "Show the possible values for each cell"
msgstr "Mostrar valores possíveis para cada célula"

#. Error dialog shown when starting a custom game that is not valid.
#: src/gnome-sudoku.vala:239
msgid "The puzzle you have entered is not a valid Sudoku."
msgstr "O puzzle que inseriu não é um Sudoku válido."

#: src/gnome-sudoku.vala:239
msgid "Please enter a valid puzzle."
msgstr "Insira um puzzle válido."

#: src/gnome-sudoku.vala:240 src/sudoku-printer.vala:46
msgid "Close"
msgstr "Fechar"

#. Warning dialog shown when starting a custom game that has multiple solutions.
#: src/gnome-sudoku.vala:248
msgid "The puzzle you have entered has multiple solutions."
msgstr "O puzzle que inseriu tem várias soluções."

#: src/gnome-sudoku.vala:248
msgid "Valid Sudoku puzzles have exactly one solution."
msgstr "Jogos de sudoku válidos só têm uma solução."

#: src/gnome-sudoku.vala:249
msgid "_Back"
msgstr "_Recuar"

#: src/gnome-sudoku.vala:250
msgid "Play _Anyway"
msgstr "Ainda assim _jogar"

#: src/gnome-sudoku.vala:292
#, c-format
msgid "Well done, you completed the puzzle in %d minute!"
msgid_plural "Well done, you completed the puzzle in %d minutes!"
msgstr[0] "Bem jogado, terminou o puzzle em %d minuto!"
msgstr[1] "Bem jogado, terminou o puzzle em %d minutos!"

#: src/gnome-sudoku.vala:296
msgid "Quit"
msgstr "Sair"

#: src/gnome-sudoku.vala:297
msgid "Play _Again"
msgstr "Jogar _novamente"

#: src/gnome-sudoku.vala:407
msgid "Reset the board to its original state?"
msgstr "Repor o tabuleiro no seu estado original?"

#: src/gnome-sudoku.vala:408
msgid "No"
msgstr "Não"

#: src/gnome-sudoku.vala:409
msgid "Yes"
msgstr "Sim"

#. Appears on the About dialog. %s is the version of the QQwing puzzle generator in use.
#: src/gnome-sudoku.vala:486
#, c-format
msgid ""
"The popular Japanese logic puzzle\n"
"\n"
"Puzzles generated by QQwing %s"
msgstr ""
"O popular puzzle lógico Japonês\n"
"\n"
"Puzzles gerados por QQwing %s"

#: src/gnome-sudoku.vala:496
msgid "translator-credits"
msgstr ""
"Duarte Loreto <happyguy_pt@hotmail.com>\n"
"Tiago S. <almosthumane@portugalmail.pt>\n"
"Pedro Albuquerque <palbuquerque73@gmail.com>\n"
"Hugo Carvalho <hugokarvalho@hotmail.com>\n"
"Juliano de Souza Camargo <julianosc@pm.me>"

#: src/number-picker.vala:87
msgid "Clear"
msgstr "Limpar"

#. Error message if printing fails
#: src/sudoku-printer.vala:45
msgid "Error printing file:"
msgstr "Erro ao imprimir o ficheiro:"

#: src/sudoku-window.vala:156
msgid "Select Difficulty"
msgstr "Selecione a dificuldade"

#: src/sudoku-window.vala:219
msgid "Create Puzzle"
msgstr "Criar puzzle"

#~ msgid "_Create your own puzzle"
#~ msgstr "_Crie o seu próprio jogo"

#~ msgid "_Cancel"
#~ msgstr "_Cancelar"

#~ msgid "Paused"
#~ msgstr "Em pausa"

#~ msgid "gnome-sudoku"
#~ msgstr "gnome-sudoku"

#~ msgid "Reset the board to its original state"
#~ msgstr "Repor o tabuleiro no seu estado original"

#~ msgid "Start a new puzzle"
#~ msgstr "Iniciar um novo jogo"

#~ msgid "_Start Playing"
#~ msgstr "_Começar a jogar"

#~ msgid "_Resume"
#~ msgstr "_Retomar"
